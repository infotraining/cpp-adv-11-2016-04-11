#define CATCH_CONFIG_MAIN

#include <iostream>
#include <vector>
#include <list>
#include <boost/range/adaptors.hpp>
#include "catch.hpp"

using namespace std;

enum DayOfWeek : uint8_t;  // forward declaration

void foo(DayOfWeek dw);

enum DayOfWeek : uint8_t
{
    Mon, Tue, Wed, Thu, Fri = 253, Sat, Sun
};

TEST_CASE("enum underlying type")
{
    static_assert(is_same<underlying_type<DayOfWeek>::type, uint8_t>::value, "Error");
}


enum class CoffeeType : uint8_t
{
    espresso, latte, cappucino
};

enum class EngineType : uint8_t
{
    petrol, diesel, wenkel
};

TEST_CASE("scoped enumerations")
{
    CoffeeType coffee = CoffeeType::cappucino;

    coffee = static_cast<CoffeeType>(0);

    using CoffeeValue = underlying_type<CoffeeType>::type;

    CoffeeValue value = static_cast<CoffeeValue>(CoffeeType::latte);

//    if (coffee == EngineType::diesel)  // compilation error
//        cout << "Bad comparison" << endl;
}

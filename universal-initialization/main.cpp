#define CATCH_CONFIG_MAIN

#include <iostream>
#include <vector>
#include <list>
#include <map>
#include <boost/range/adaptors.hpp>
#include <map>
#include "catch.hpp"

using namespace std;

struct Point
{
    int x = 0;
    int y{0};

    Point(int x, int y) : x{x}, y{y}
    {
    }

    Point() = default;
};

TEST_CASE("uniform initializaiton syntax")
{
    int x{6578};
    short sx{static_cast<short>(x)};

    Point pt1 = {10, 20};
    Point pt2{20, 30};

    map<string, int> dict = { {"one", 1}, {"two", 2} };

    REQUIRE(dict["one"] == 1);

    Point pt;
    REQUIRE((pt.x == 0 && pt.y == 0));
}


void show_item(initializer_list<int> il)
{
    cout << "il's size: " << il.size() << endl;

    for(const auto& item : il)
        cout << item << " ";
    cout << endl;
}

TEST_CASE("initializer list")
{
    show_item({1, 2, 3, 4, 5});
}


TEST_CASE("initializer_list as constructor")
{
    vector<int> vec = { 1, 2, 3, 4, 5 };

    SECTION("call with ()")
    {
        vector<int> vec1(10, -1);

        REQUIRE(vec1.size() == 10);
        REQUIRE(all_of(vec1.begin(), vec1.end(), [](int x) { return x == -1;}));
    }

    SECTION("call with {}")
    {
        vector<int> vec1{10, -1};

        REQUIRE(vec1.size() == 2);
        REQUIRE(vec1.front() == 10);
        REQUIRE(vec1.back() == -1);

        vector<string> words{10, "-1"};
    }
}

template <typename Value>
using DictKeyStr = map<string, Value>;


template <typename T, typename R>
auto multiply(const T& a, const T& b) //-> decltype(a * b)
{
    using ResultType = decltype(a * b);

    return ResultType{a * b};
}


TEST_CASE("decltype")
{
    DictKeyStr<int> dict = { {"one", 1}, {"two", 2} };

    auto other = dict; // copy
    decltype(dict) other2; // empty dict

    auto one = other2["one"];


    REQUIRE(one == 0);
    REQUIRE(other2.size() == 1);
}

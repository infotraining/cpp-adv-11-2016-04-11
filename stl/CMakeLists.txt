get_filename_component(PROJECT_NAME_STR ${CMAKE_SOURCE_DIR} NAME)
string(REPLACE " " "_" ProjectId ${PROJECT_NAME_STR})

cmake_minimum_required(VERSION 2.8)

project(${PROJECT_NAME_STR})

cmake_minimum_required(VERSION 2.8)
aux_source_directory(. SRC_LIST)
add_definitions(-std=c++14)
find_package(Threads)
add_executable(${PROJECT_NAME} ${SRC_LIST})
target_link_libraries(${PROJECT_NAME} ${CMAKE_THREAD_LIBS_INIT})

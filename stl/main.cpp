#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <deque>
#include <list>
#include <array>
#include <set>
#include <unordered_set>
#include <boost/functional/hash.hpp>

using namespace std;

TEST_CASE("deque")
{
    deque<int> dq = { 1, 2, 3, 5, 6};

    SECTION("item can be pushed at front")
    {
        dq.push_front(4);

        REQUIRE(dq[0] == 4);
    }

    SECTION("has random_access_iterators")
    {
        auto it1 = dq.begin();
        auto it2 = it1 + 3;

        REQUIRE(*it2 == 5);
        REQUIRE(it2 - it1 == 3);
    }
}

TEST_CASE("list")
{
    list<int> lst = { 6, 23, 6, 2, 5, 8, 0, 4, 6, 6, 1 };

    SECTION("insert or remove has constant time")
    {
        auto it = find(lst.begin(), lst.end(), 0);

        lst.erase(it);

        REQUIRE(none_of(lst.begin(), lst.end(), [](int x) { return x == 0;}));
    }

    SECTION("has specialized operations")
    {
        SECTION("sort asc")
        {
            lst.sort();

            REQUIRE(is_sorted(lst.begin(), lst.end()));

            SECTION("unique")
            {
                lst.unique();

                REQUIRE(count(lst.begin(), lst.end(), 6) == 1);
            }
        }

        SECTION("sort desc")
        {
            auto gt = [](int x, int y) { return  x > y;};

            lst.sort(gt);

            REQUIRE(is_sorted(lst.begin(), lst.end(), gt));
        }
    }
}

void legacy_code(int tab[], size_t size)
{
}

TEST_CASE("array")
{
    int tab[255] = { 1, 2, 3, 4, 5 };

    SECTION("replaces native array")
    {
        array<int, 255> arr = { 1, 2, 3, 4, 5 };

        SECTION("has interface of container")
        {
            REQUIRE(arr.size() == 255);
            REQUIRE(*(arr.begin()) == 1);
            REQUIRE_THROWS_AS(arr.at(300), out_of_range);
        }

        SECTION("has data() member")
        {
            legacy_code(arr.data(), arr.size());
        }

        SECTION("fill()")
        {
            arr.fill(-1);
        }

        SECTION("get<Index> can get item in compile time")
        {
            const auto& item = get<3>(arr);

            REQUIRE(item == 4);get<3>(arr);
        }
    }
}

TEST_CASE("set")
{
    set<int> s = { 7, 2, 6, 7, 23, 54, 1, 2 };

    SECTION("sorts ascending by default")
    {
        s.insert(8);
        s.insert(9);

        REQUIRE(is_sorted(s.begin(), s.end()));
    }

    SECTION("find has log complexity")
    {
        auto it = s.find(7);

        REQUIRE(it != s.end());
        REQUIRE(*it == 7);
    }

    SECTION("erase has log complexity")
    {
        s.erase(54);

        REQUIRE(s.count(54) == 0);
    }

    SECTION("lower_bound and upper_bound")
    {
        multiset<int> ms(s.begin(), s.end());

        SECTION("when item is in container")
        {
            ms.insert({3, 3, 3, 4, 1, 1, 66});

            auto start3 = ms.lower_bound(3);
            auto end3 = ms.upper_bound(3);

            for(auto it = start3; it != end3; ++it)
                cout << *it << " ";
            cout << endl;
        }

        SECTION("when item is not present in container")
        {
            auto start3 = ms.lower_bound(3);
            auto end3 = ms.upper_bound(3);

            REQUIRE(start3 == end3);

            ms.insert(start3, 3); // insert with iterator
        }

        SECTION("equal_range")
        {
            auto pair_it = ms.equal_range(3);

            REQUIRE(pair_it.first == pair_it.second);
        }
    }
}

TEST_CASE("map")
{
    map<int, string> dict = { {1, "one"}, {4, "four"} };

    SECTION("has [] operator")
    {
        REQUIRE(dict[1] == "one");

        SECTION("with side-effect")
        {
            dict[2];

            auto it = dict.find(2);

            REQUIRE(it != dict.end());
            REQUIRE(it->second == "");
        }
    }

    SECTION("at()")
    {
       REQUIRE_THROWS_AS(dict.at(2), out_of_range);
    }
}

void print_stat(const unordered_set<int>& us)
{
    cout << "us.bucket_count: " << us.bucket_count()
         << " us.load_factor: " << us.load_factor() << endl;
}

TEST_CASE("unordered_set")
{
    random_device rd;
    mt19937 gen{rd()};
    uniform_int_distribution<> dist{1, 1000};

    unordered_set<int> uset(500);

    //uset.max_load_factor(0.5);

    print_stat(uset);

    auto prev_bucket_count = uset.bucket_count();

    for(int i = 0; i < 500; ++i)
    {
        uset.insert(dist(gen));

        if (prev_bucket_count != uset.bucket_count())
        {
            print_stat(uset);
            prev_bucket_count = uset.bucket_count();
        }
    }

    cout << "uset.size(): " << uset.size() << endl;
    cout << "uset.load_factor(): " << uset.load_factor() << endl;       
}

template <typename InputIterator, typename OutputIterator>
OutputIterator my_copy(InputIterator start, InputIterator end, OutputIterator out)
{
    for(auto it = start; it != end; ++it)
        *(out++) = *it;

    return out;
}

TEST_CASE("algorithms")
{
    SECTION("type of iterators")
    {
        vector<int> vec = { 1, 2, 3, 4, 5 };
        list<int> lst(vec.size());

        my_copy(vec.begin(), vec.end(), lst.begin());

        list<int> evens;

        copy_if(vec.begin(), vec.end(),
                back_inserter(lst), [](int x) { return x % 2 == 0;});
    }

    SECTION("remove")
    {
        vector<int> vec = { 5, 12, 7, 234, 123, 7, 534, 7, 22, 7 };

        auto prev_size = vec.size();

        auto garbage_start = remove(vec.begin(), vec.end(), 7);
        vec.erase(garbage_start, vec.end());

        for(const auto& item : vec)
            cout << item << " ";
        cout << endl;

        REQUIRE(none_of(vec.begin(), vec.end(), [](int x) { return x == 7;}));
    }
}

class Person
{
    mutable size_t hash_cache_;
    friend class HashPerson;
    friend class std::hash<Person>;
public:
    int id_;
    string first_name_;
    string last_name_;

    Person(int id, const string& fn, const string& ln)
        : id_{id}, first_name_{fn}, last_name_{ln}
    {
    }

    void info() const
    {
        cout << "Person(id: " << id_ << ", " << first_name_
             << " " << last_name_ << ")" << endl;
    }

    bool operator==(const Person& p) const
    {
        return (id_ == p.id_) && (first_name_ == p.first_name_) && (last_name_ == p.last_name_);
    }
};

struct HashPerson
{
public:
    size_t operator()(const Person& p) const
    {
//        size_t h1 = hash<int>{}(p.id_);
//        auto string_hash = hash<string>{};
//        size_t h2 = string_hash(p.first_name_);
//        size_t h3 = string_hash(p.last_name_);

//        return h1 ^ (h2 << 1) ^ (h3 << 2);

        size_t seed = 0;
        boost::hash_combine(seed, p.id_);
        boost::hash_combine(seed, p.first_name_);
        boost::hash_combine(seed, p.last_name_);

        p.hash_cache_ = seed;

        return seed;
    }
};

namespace std
{
    template <>
    class hash<Person>
    {
    public:
        using argument_type = Person;
        using result_type = size_t;

        result_type operator()(const argument_type& p) const
        {
            if (p.hash_cache_ == 0)
            {
                size_t seed = 0;
                boost::hash_combine(seed, p.id_);
                boost::hash_combine(seed, p.first_name_);
                boost::hash_combine(seed, p.last_name_);

                p.hash_cache_ = seed;
            }

            return p.hash_cache_;
        }
    };
}

TEST_CASE("custom type as key unordered containers")
{
    SECTION("hashing using custom hasher")
    {
        unordered_set<Person, HashPerson> people;

        people.insert(Person(1, "Jan", "Kowalski"));
        people.emplace(4, "Adam", "Nowak");
        people.emplace(2, "Ewa", "Kowalska");
    }

    SECTION("hashing using specialized std::hash<Person>")
    {
        unordered_set<Person> people;

        people.insert(Person(1, "Jan", "Kowalski"));
        people.emplace(4, "Adam", "Nowak");
        people.emplace(2, "Ewa", "Kowalska");
    }
}

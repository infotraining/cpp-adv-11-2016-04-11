#include <iostream>
#include <chrono>
#include <map>

using namespace std;

auto get_name(int id)  // return by copy
{
    if (id == 1)
        return "Gadget"s;
    else if (id == 2)
        return "SuperGadget"s;

    return string("Unknown");
}

// return by ref
decltype(auto) /*pair<const int, string>&*/ find_item(map<int, string>& dict, int key)
{
    auto where = dict.find(key);

    if (where != dict.end())
        return *where;

    throw std::out_of_range("key not found");
}

int main()
{
    auto name = get_name(1);
    cout << name << endl;

    auto time_interval = chrono::milliseconds{500};
    time_interval = 100ms;

    string (*ptr_fun)(int) = &get_name;


    map<int, string> dict = { {1, "one"}, {2, "two"} };

    auto& item = find_item(dict, 2);
    item.second = "dwa";
    cout << "found: " << item.second << endl;
    cout << "in dict: " << dict[2] << endl;
}

# Books

* Modern C++ Programming with Test-Driven Development
* Effective Modern C++
* The C++ Programming Language, Fourth Edition
* A Tour of C++

# Video
* https://www.youtube.com/watch?v=1OEu9C51K2A
* https://www.youtube.com/watch?v=hEx5DNLWGgA
* https://www.youtube.com/watch?v=xnqTKD8uD64

# CppCoreGuidelines
* https://github.com/isocpp/CppCoreGuidelines

# constexpr
* https://www.youtube.com/watch?v=fZjYCQ8dzTc
* https://www.youtube.com/watch?v=qO-9yiAOQqc

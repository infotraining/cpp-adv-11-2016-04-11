#ifndef CLASS_HPP
#define CLASS_HPP

#include <cstddef>

class Vector
{
public:
    size_t size() const;    
};

#endif

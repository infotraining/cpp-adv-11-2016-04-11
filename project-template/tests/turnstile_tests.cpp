#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include "../src/vector.hpp"


TEST_CASE("Catch demo")
{
    SECTION("Vector - default size is zero")
    {
        Vector vec;

        REQUIRE(vec.size() == 0);
    }
}

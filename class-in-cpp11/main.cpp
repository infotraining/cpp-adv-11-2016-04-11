#include <iostream>
#include <set>

using namespace std;

class NoCopyable
{
protected:
    NoCopyable() = default;
public:
    NoCopyable(const NoCopyable&) = delete;
    NoCopyable& operator=(const NoCopyable&) = delete;
};

int generate_id()
{
    static int counter;

    return ++counter;
}

class Gadget : NoCopyable
{
    int id_ = generate_id();
    string name_ {"unknown"};
public:
    Gadget(int id, const string& name) : id_{generate_id()}, name_{name}
    {
    }

    Gadget(int id) : Gadget{id, "not-known"}
    {
    }

    Gadget() = default;

    virtual ~Gadget() =  default;

    int id() const
    {
        return id_;
    }

    string name() const
    {
        return name_;
    }

    virtual void run() const
    {
        cout << "Gadget(" << id_
             << ", " << name_ << ") is running..." << endl;
    }
};

class SuperGadget : public Gadget
{
public:
    using Gadget::Gadget;

    void run() const override final
    {
        cout << "SuperGadget(" << id()
             << ", " << name() << ") is running better..." << endl;
    }
};

class HyperGadget : public SuperGadget
{
public:
//    void run() const override // illegal
//    {
//    }
};


void integral_only(int x)
{
    cout << "foo(int: " << x << ")" << endl;
}

void integral_only(double) = delete;


class IndexedSet final : public set<int>
{
public:
    using set<int>::set;  // inheritance of constructors from base class

    IndexedSet(std::initializer_list<int> il) : set<int>(il)
    {
        cout << "customized constructor with il" << endl;
    }

    const int& operator[](size_t index) const
    {
        auto it = begin();
        for(size_t i = 0; i < index; ++i)
            ++it;

        return *it;
    }
};


int main() try
{
    Gadget g1{-1};
    g1.run();

    //Gadget cg = g;
    //g1 = g;

    Gadget g2{1, "ipad"};
    g2.run();

    SuperGadget g;
    Gadget& ref_g = g;
    ref_g.run();

    integral_only(10);
    short sx = 7;
    integral_only(sx);
    //integral_only(3.14);  // compilation error
    //integral_only(3.14F); // compilation error

    IndexedSet my_set = { 1, 7, 235, 3, 5, 6, 2 };

    cout << my_set[0] << " - " << my_set[6] << endl;

}
catch(...)
{
    cout << "Caught exception..." << endl;
}

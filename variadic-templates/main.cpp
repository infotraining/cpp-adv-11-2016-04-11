#include <iostream>
#include <vector>
#include <array>

using namespace std;

//---------------------------------------------

void print()
{
    cout << endl;
}

template <typename T, typename... Tail>
void print(const T& arg1, const Tail&... params)
{
    cout << arg1 << " ";
    print(params...);
}

//---------------------------------------------

template <typename... Types>
struct Count;

template <typename Head, typename... Tail>
struct Count<Head, Tail...>
{
    static const int value = 1 + Count<Tail...>::value;
};

template <>
struct Count<>
{
    static const int value = 0;
};

template <typename... Types>
struct ValidateCount
{
    static_assert(Count<Types...>::value == sizeof...(Types), "Error in counting of parameters");
};

//-----------------------------------------------

template <typename... Bases>
struct Derived : public Bases...
{

};

class A {};
class B {};
class C {};

Derived<A, B, C> d;


class VectorWrapper
{
    vector<int> vec_;
public:
    template <typename... Args>
    VectorWrapper(Args&&... args) : vec_(forward<Args>(args)...)
    {
    }

    VectorWrapper(const VectorWrapper&) = default;

    VectorWrapper(VectorWrapper& source) : vec_(source.vec_)
    {}

    void show() const
    {
        cout << "vec: [ ";
        for(const auto& item : vec_)
        {
            cout << item << " ";
        }
        cout << "]" << endl;
    }
};

struct details
{
    template <typename T>
    constexpr static auto sum(T&& arg)
    {
        return arg;
    }


    template <typename T, typename... Args>
    constexpr static auto sum(T&& arg, Args&&... args)
    {
        return arg + sum(forward<Args>(args)...);
    }
};

template <typename... Args>
constexpr auto avg(Args&&... args)
{
    return details::sum(forward<Args>(args)...) / sizeof...(args);
}

constexpr long factorial(long n)
{
    if (n == 0)
        return 1;
    return factorial(n - 1) * n;
}

int main()
{
    print(3);

    print(5, 34, "text");

    print(9, 43.4, "str"s, "text");

    static_assert(Count<int, double, string>::value == 3, "Wrong number");

    ValidateCount<int, int, char, string>{};

    VectorWrapper v1(10);  // vector<int>(10)
    v1.show();

    VectorWrapper v2(10, -1); // vector<int>(10, -1)
    v2.show();

    auto il = { 1, 2, 3, 4 };
    VectorWrapper v3(il);   // vector<int>({1, 2, 3, 4})
    v3.show();

    vector<int> vec = {6, 7, 8};
    VectorWrapper v4(vec);
    v4.show();

    VectorWrapper v5 = v4;

    // TODO

    auto avg1 = avg(1, 3);
    cout << "avg1: " << avg1 << endl;

    auto avg2 = avg(1.0, 5, 234, 6.0);
    cout << "avg2: " << avg2 << endl;

    static_assert(avg(1, 3) == 2, "Error");

    array<int, factorial(8)> arr;

    cout << "arr.size() = " << arr.size() << endl;
}

#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <iostream>
#include <vector>
#include <set>
#include <memory>
#include <queue>
#include <cstdlib>

using namespace std;

class Gadget
{
    int id_;
public:
    Gadget()
    {
        static int seed = 300;
        id_ = ++seed;
    }

    Gadget(int id) : id_{id}
    {
        cout << "Gadget(" << id_ << ")" << endl;
    }

    ~Gadget()
    {
        cout << "~Gadget(" << id_ << ")" << endl;
    }

    void use()
    {
        cout << "using gadget: " << id_ << endl;
    }
};

void use(Gadget& g)
{
    g.use();
}

void use(Gadget* g)
{
    g->use();
}

void use_and_destroy(unique_ptr<Gadget> gptr)
{
    gptr->use();
}


unique_ptr<Gadget> create_gadget()
{
    static int counter = 100;

    return make_unique<Gadget>(++counter);
}


TEST_CASE("unique_ptr")
{
    SECTION("controls lifetime of object")
    {
        unique_ptr<Gadget> g0{new Gadget(0)};

        // c++14
        unique_ptr<Gadget> g1 = make_unique<Gadget>(1);

        g1->use();
        use(*g1);
    }

    SECTION("is only moveable")
    {
        unique_ptr<Gadget> g1 = make_unique<Gadget>(2);
        unique_ptr<Gadget> g2 = move(g1);

        REQUIRE(g1.get() == nullptr);
        g2->use();
    }

    SECTION("can be returned from functions")
    {
        unique_ptr<Gadget> g3 = create_gadget();
        g3->use();
    }

    SECTION("can be stored in container")
    {
        unique_ptr<Gadget> uptr = make_unique<Gadget>(55);

        vector<unique_ptr<Gadget>> gadgets;

        gadgets.push_back(make_unique<Gadget>(99));
        gadgets.push_back(create_gadget());
        gadgets.emplace_back(new Gadget(98));
        gadgets.push_back(move(uptr));

        for(const auto& ptr_gadget : gadgets)
            ptr_gadget->use();

        {
            auto ptr = move(gadgets[0]);
            ptr->use();
        }

        cout << "after scope..." << endl;
    }

    SECTION("memleak in c++98")
    {
        use(new Gadget(10));
    }

    SECTION("no memleak in c++11")
    {
        unique_ptr<Gadget> uptr = make_unique<Gadget>(55);

        use(uptr.get());

        use_and_destroy(make_unique<Gadget>(10));
        use_and_destroy(create_gadget());
        use_and_destroy(move(uptr));
    }

    SECTION("can control liftime of dynamic tables")
    {
        unique_ptr<Gadget[]> gadgets = make_unique<Gadget[]>(10);

        gadgets[6].use();
    }
}


TEST_CASE("shared_ptr")
{
    SECTION("share ownership")
    {
        unique_ptr<Gadget> uptr = make_unique<Gadget>(55);

        shared_ptr<Gadget> g;

        {
            vector<shared_ptr<Gadget>> gadgets;
            gadgets.push_back(make_shared<Gadget>(99));
            gadgets.push_back(create_gadget());
            gadgets.emplace_back(new Gadget(98));
            gadgets.push_back(move(uptr));

            g = gadgets[0];
            cout << "use_count for g: " << g.use_count() << endl;
        }

        cout << "after scope" << endl;
        g->use();
        cout << "use_count for g: " << g.use_count() << endl;
    }
}

TEST_CASE("custom dealloc")
{
    SECTION("unique_ptr")
    {
        unique_ptr<FILE, int(*)(FILE*)> f(fopen("test1.txt", "w+"), &fclose);

        fprintf(f.get(), "tekst");
    }

    SECTION("shared_ptr")
    {
        shared_ptr<FILE> f(fopen("test2.txt", "w+"), [](FILE* f) { if (f) fclose(f);});

        fprintf(f.get(), "tekst");
    }
}

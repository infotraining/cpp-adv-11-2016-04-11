#include <iostream>
#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <vector>
#include <iterator>
#include <memory>

using namespace std;

// find_null: Given a container of pointers, return an
// iterator to the first null pointer (or the end
// iterator if none is found)

template <typename Container>
auto find_null(Container& c) //-> decltype(begin(c))
{
    auto it = begin(c);

    while(it != end(c))
    {
        if (*it == nullptr)
            return it;

        ++it;
    }

    return it;
}

TEST_CASE("find_null")
{
    SECTION("vector of raw-pointers")
    {
        int x = 10;
        vector<int*> vec_ptrs = { &x, nullptr, &x };

        auto where_null = find_null(vec_ptrs);

        REQUIRE(distance(vec_ptrs.begin(), where_null) == 1);
    }

    SECTION("array of raw-pointers")
    {
        int x;
        int* tab[4] = { &x, &x, &x };

        auto where_null = find_null(tab);

        REQUIRE(distance(begin(tab), where_null) == 3);
    }

    SECTION("vector of shared_ptrs")
    {
        vector<shared_ptr<int>> vec_ptrs = { make_shared<int>(1), make_shared<int>(2),
                                             nullptr, make_shared<int>(3) };

        auto where_null = find_null(vec_ptrs);

        REQUIRE(distance(vec_ptrs.begin(), where_null) == 2);
    }
}

//int main()
//{



	/*
	const vector<int*> vec_ptr = { new int{10}, nullptr, new int{20} };

    auto where_null1 = find_null(vec_ptr);

    assert(distance(vec_ptr.begin(), where_null1) == 1);

    int* tab[] = { vec_ptr[0], vec_ptr[0], vec_ptr[2], nullptr };

    auto where_null2 = find_null(tab);

    assert(distance(begin(tab), where_null2) == 3);

    vector<shared_ptr<int>> vec_sptr = { make_shared<int>(10), nullptr, make_shared<int>(20) };

    auto where_null3 = find_null(vec_sptr);

    assert(distance<decltype(where_null3)>(vec_sptr.begin(), where_null3) == 1);
    */
//}

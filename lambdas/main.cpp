#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <iostream>
#include <vector>
#include <set>
#include <memory>
#include <queue>

using namespace std;

class Lambda_4623457
{
public:
    void operator()() const
    {
        cout << "simplest lambda\n";
    }
};

class Lambda_2374627
{
public:
    bool operator()(int x) const
    {
        return x % 2 == 0;
    }
};

//[value](int x) { return x % value == 0; }
class Lambda_2345267
{
    const int value;
public:
    Lambda_2345267(int value) : value{value}
    {}

    bool operator()(int x) const
    {
        return x % value == 0;
    }
};


template <typename InputIter, typename Callable>
InputIter my_find_if(InputIter start, InputIter end, Callable predicate)
{
    for(auto it = start; it != end; ++it)
    {
        if (predicate(*it))
            return it;
    }

    return end;
}

int global_value = 10;

auto create_generator(int seed)
{
    return [seed]() mutable { return ++seed; };
}


TEST_CASE("Lambda")
{
    SECTION("basics")
    {
        [](){}();

        auto simplest_lambda = []{ cout << "simplest lambda\n"; };
        simplest_lambda();

        auto is_even = [](int x) { return x % 2 == 0; };

        REQUIRE(is_even(2));
        REQUIRE_FALSE(is_even(1));
    }


    SECTION("using in stl algorithms")
    {
        vector<int> vec = {1, 3, 5, 8, 9, 11 };

        auto first_even = my_find_if(vec.begin(), vec.end(), [](int x) { return x % 2 == 0;} );

        REQUIRE(first_even != vec.end());
        REQUIRE(*first_even == 8);
    }

    SECTION("captures")
    {
        int value = 2;
        int sum = 0;
        vector<int> vec = { 1, 2, 3 };

        SECTION("by value")
        {
            auto predicate = [value](int x) { return x % value == 0; };
        }

        SECTION("by reference")
        {
            for_each(vec.begin(), vec.end(), [&sum](int x) { sum += x; });
        }

        SECTION("many captured vars")
        {
            for_each(vec.begin(), vec.end(), [&sum, value](int x) { sum += x * value; });
        }

        SECTION("default capture types")
        {
            auto l1 = [=](int x) { return x * global_value; };  // global_value is not captured
            auto l2 = [&, value](int x) { return sum += x * value; };
        }
    }

    SECTION("mutable lambda")
    {
        int seed = 1;

        auto gen = [seed]() mutable { return ++seed; };

        vector<int> vec(5);
        generate(vec.begin(), vec.end(), create_generator(1));

        auto expected = { 2, 3, 4, 5, 6 };
        REQUIRE(equal(vec.begin(), vec.end(), expected.begin()));


        REQUIRE(gen() == 2);
        REQUIRE(gen() == 3);
        REQUIRE(gen() == 4);
    }


    SECTION("lambda type")
    {
        auto compare_by_value =
                [](const shared_ptr<int>& a, const shared_ptr<int>& b)
                {
                    return *a < *b;
                };

        set<shared_ptr<int>, decltype(compare_by_value)> my_set(compare_by_value);

        my_set.insert(make_shared<int>(9));
        my_set.insert(make_shared<int>(2));
        my_set.insert(make_shared<int>(5));
        my_set.insert(make_shared<int>(1));

        for(const auto& item : my_set)
            cout << *item << " ";
        cout << endl;
    }

    SECTION("storing capture object")
    {
        auto compare_by_value =
                [](const shared_ptr<int>& a, const shared_ptr<int>& b)
                {
                    return *a < *b;
                };

        function<bool (const shared_ptr<int>&, const shared_ptr<int>&)> fcompare =
                [](const shared_ptr<int>& a, const shared_ptr<int>& b)
                {
                    return *a < *b;
                };
    }
}

void foo(int a)
{
    cout << "foo(" << a << ")" << endl;
}

class Foo
{
public:
    void operator()(int a)
    {
        cout << "Foo(" << a << ")" << endl;
    }
};

TEST_CASE("std::function")
{
    function<void(int)> f;

    SECTION("stores pointer to function")
    {
        f = &foo;
        f(10);
    }

    SECTION("stores functor")
    {
       f = Foo();
       f(20);
    }

    SECTION("stores lambda")
    {
        f = [](int a) { cout << "lambda(" << a << ")" << endl; };
        f(30);
    }
}

using WorkItem = function<void()>;

class WorkQueue
{
private:
    queue<WorkItem> q_;
public:
    template <typename Func>
    void submit(Func&& f)  // univeral reference
    {
        q_.push(forward<Func>(f));
    }

    bool empty() const
    {
        return q_.empty();
    }

    void run()
    {
        while(!q_.empty())
        {
            WorkItem task = q_.front();
            task(); // calling a task
            q_.pop();
        }
    }
};


TEST_CASE("WorkQueue")
{
    WorkQueue q;

    SECTION("when constructed is empty")
    {
        REQUIRE(q.empty());
    }

    SECTION("when work submitted is not empty")
    {
        q.submit([]{});

        REQUIRE_FALSE(q.empty());
    }

    SECTION("run()")
    {
        bool a_called = false;
        bool b_called = false;

        q.submit([&a_called] { a_called = true;});
        q.submit([&b_called] { b_called = true;});

        REQUIRE_FALSE(a_called);
        REQUIRE_FALSE(b_called);

        q.run();

        SECTION("calls all submitted functions")
        {
            REQUIRE(a_called);
            REQUIRE(b_called);
        }

        SECTION("after queue is empty")
        {
            REQUIRE(q.empty());
        }
    }
}


class Printer
{
public:
    void on()
    {
        cout << "printer is on..." << endl;
    }

    void print(const string& content)
    {
        cout << "printing " << content << endl;
    }

    void off()
    {
        cout << "printer is off" << endl;
    }

};

TEST_CASE("WorkQueue with printer")
{
    Printer prn;

    WorkQueue q;

    string doc = "document...";

    q.submit([&prn] { prn.on();});
    q.submit([&prn, doc] { prn.print(doc);});
    q.submit([&prn] { prn.off();});

    q.run();
}

class Lambda_98787863
{
public:
    template <typename T>
    auto operator()(T x) { return x % 2 == 0; }
};

class Lambda_53698345
{
    unique_ptr<int> uptr;
public:
    Lambda_53698345(unique_ptr<int> uptr) : uptr{move(uptr)}
    {}

    auto operator()() { cout << *uptr << "\n"; }
};

TEST_CASE("Lambda in C++14")
{
    int value = 2;
    int sum = 0;
    vector<string> vec = { "1", "2", "3" };

    SECTION("auto as paramter type")
    {
        auto predicate = [value](const auto& x) { return x % value == 0; };
    }

    SECTION("move expresion for closures")
    {
        unique_ptr<int> uptr = make_unique<int>(5);

        auto l1 = [uptr = move(uptr)]() { cout << *uptr << "\n"; };
    }
}

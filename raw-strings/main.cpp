#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <iostream>
#include <string>

using namespace std;

TEST_CASE("raw strings literals")
{
    SECTION("new lines are easile inserted")
    {

        string expected = "Line1\nLine2";

        string raw = R"(Line1
Line2)";

        REQUIRE(expected == raw);
    }

    SECTION("escape characters are ignored")
    {
        string expected = "c:\\path\\n";

        string raw = R"(c:\path\n)";

        REQUIRE(expected == raw);
    }

    SECTION("delimeters are customizable")
    {
        string expected = "c:\\path(*)\\n";

        string raw1 = R"abcd(c:\path(*)\n)abcd";
        string raw2 = R"%(c:\path(*)\n)%";

        REQUIRE(expected == raw1);
        REQUIRE(expected == raw2);
    }
}

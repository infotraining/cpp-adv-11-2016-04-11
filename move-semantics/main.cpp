#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <iostream>
#include <string>

using namespace std;

class Moveable
{
    int* buffer_;
    size_t size_;
    string name_;
public:
    Moveable(size_t size, const string& name, int value)
        : buffer_{new int[size]}, size_{size}, name_{name}
    {
        cout << "Movebale(" << name_ << ")" << endl;
        fill(buffer_, buffer_ + size_, value);
    }

    Moveable(const Moveable& other)
        : buffer_{new int[other.size_]}, size_{other.size_}, name_{other.name_}
    {
        cout << "Movebale(const Moveable& " << name_ << ")" << endl;
        copy(other.buffer_, other.buffer_ + size_, buffer_);
    }

    Moveable& operator=(const Moveable& other)
    {
        if (this != &other)
        {
            cout << "operator=(const Moveable& " << name_ << ")" << endl;

            int* temp_buffer = new int[other.size_];
            copy(other.buffer_, other.buffer_ + other.size_, temp_buffer);
            delete[] buffer_;

            buffer_ = temp_buffer;
            size_ = other.size_;
            name_ = other.name_;
        }

        return *this;
    }

    Moveable(Moveable&& source) noexcept
        : buffer_{source.buffer_}, size_{source.size_}, name_{move(source.name_)}
    {
        cout << "Movebale(Moveable&& " << name_ << ")" << endl;

        source.buffer_ = nullptr;
        source.size_ = 0;
    }

    Moveable& operator=(Moveable&& source) noexcept
    {
        cout << "operator=(Moveable&& " << name_ << ")" << endl;

        if (this != &source)
        {
            buffer_ = source.buffer_;
            size_ = source.size_;
            name_ = move(source.name_);

            // clear source
            source.buffer_ = nullptr;
            source.size_ = 0;
        }

        return *this;
    }

    void reset_value(int value)
    {
        fill(buffer_, buffer_ + size_, value);
    }

    void show()
    {
        cout << name_ << " [ ";
        for(size_t i = 0; i < size_; ++i)
            cout << buffer_[i] << " ";
        cout << "]" << endl;
    }


    ~Moveable()
    {
        cout << "~Movebale(" << name_ << ")" << endl;
        delete[] buffer_;
    }
};


Moveable create_moveable(const string& name)
{
    static int value_gen = 0;

    Moveable mv{10, name, -1};
    cout << "creating ";
    mv.reset_value(++value_gen);
    mv.show();

    return mv;
}

class C1
{
public:
    vector<int> vec;
    string name;

    C1(const vector<int>& v, const string& name) : vec{v}, name{name}
    {}
};

class C2
{
public:
    vector<int> vec;
    string name;

    C2(const vector<int>& v, const string& name) : vec{v}, name{name}
    {}

    ~C2()
    {
    }

    C2(C2&&) = default;
    C2& operator=(C2&&) = default;

    C2(const C2&) = default;
    C2& operator=(const C2&) = default;
};


TEST_CASE("lvalue - rvalue")
{
    SECTION("lvalue can be bound to l-value ref")
    {
        int x = 10;
        int& lvref = x;
    }

    SECTION("rvalue can be bound const l-value ref")
    {
        const string& lvref = string("temp");
    }

    SECTION("rvalue can be bound to r-value ref")
    {
        string&& rvref = string("temp");
        REQUIRE(rvref == "temp");
    }

    SECTION("lvalue can't be bound do r-value ref")
    {
        string text = "text";

        //string&& rvref = text;  // illegal
    }
}

TEST_CASE("Moveable")
{
    Moveable mv1{10, "mv1", 1};
    mv1.show();

    Moveable mv2 = create_moveable("mv2");

    SECTION("copying")
    {
        cout << "\ncopy-------------\n";

        Moveable mv3 = mv1;  // cc
        mv3 = mv2;  // c=
    }

    SECTION("moving")
    {
        cout << "\nmove-------------\n";

        Moveable mv3 = create_moveable("mv4");  // mvc
        mv3 = create_moveable("mv5");  // mv=
    }

    SECTION("swap")
    {
        cout << "\nswap-------------\n";

        swap(mv1, mv2);

        cout << "mv1: ";
        mv1.show();

        cout << "mv2: ";
        mv2.show();

        cout << "\n-----------------\n";
    }

    SECTION("storing in containers")
    {
        cout << "\ncontainers----------\n";
        vector<Moveable> vec;

        vec.push_back(Moveable{10, "cm1", 9});
        vec.emplace_back(11, "cm-forward", 0);
        vec.push_back(create_moveable("cm2"));


        for(int i = 3; i < 16; ++i)
            vec.push_back(create_moveable("cm" + to_string(i)));

        cout << "\n-----------------\n";
    }

    SECTION("move from const")
    {
        cout << "\nmove from const----------\n";

        const Moveable cm10{10, "cm10", 0};
        Moveable cm20 = move(cm10);
    }

    SECTION("c2")
    {
        C2 c2a{{1, 2, 3}, "c2a"};

        C2 c2b = c2a;
    }
}

class User
{
    Moveable moveable_;
public:
//    User(const Moveable& mv) : moveable_ {mv}
//    {}

//    User(Moveable&& mv) : moveable_{move(mv)}
//    {}

    User(Moveable mv) : moveable_{move(mv)}
    {
    }
};

TEST_CASE("User")
{
    Moveable mv{10, "mv", 1};
    const Moveable cmv{10, "cmv", 5};

    SECTION("user constructors")
    {
        cout << "\nUser constructors----------\n";

        User u1{mv}; // copy mv
        User u2{cmv}; // copy cmv
        User u3{Moveable{10, "temp", -1}};  // move
        User u4{move(cmv)}; // copy
    }

    SECTION("User container")
    {
        cout << "\nUser containers----------\n";

        vector<User> users;

        User user{mv};

        users.push_back(user);
        users.emplace_back(move(mv));
        users.push_back(User{Moveable{10, "temp", -1}});
    }
}


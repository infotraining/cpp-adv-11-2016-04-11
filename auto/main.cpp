#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <iostream>
#include <string>
#include <cassert>
#include <type_traits>
#include <list>
#include <algorithm>

using namespace std;

class Foo
{
};

TEST_CASE("auto")
{
    SECTION("auto can deduce type of variable from init expression")
    {
        auto i = 42;
        static_assert(is_same<decltype(i), int>::value, "Error");

        auto l = 54L;
        static_assert(is_same<decltype(l), long>::value, "Error");

        auto x = i + l;
        static_assert(is_same<decltype(x), long>::value, "Error");

        vector<int> vec = {1, 2, 3};
        auto it = vec.begin();

        static_assert(is_same<decltype(it), vector<int>::iterator>::value, "Error");
    }

    SECTION("auto is convenient during iteration")
    {
        list<int> vec = {1, 2, 3};

        auto it = vec.begin();

        while(it != vec.end())
        {
            cout << *it << " ";
            ++it;
        }
        cout << endl;
    }

    SECTION("works with any types")
    {
        auto f = Foo{};
        static_assert(is_same<decltype(f), Foo>::value, "Error");
    }

    SECTION("can be used with const modifiers and with * and &")
    {
        int x = 42;
        const auto* ptr1 = &x;
        auto ptr2 = &x;
        const auto& refx = x;

        REQUIRE(*ptr1 == x);
        REQUIRE(*ptr2 == x);
        REQUIRE(refx == x);
    }
}

TEST_CASE("auto - iteratrion over containers")
{
    vector<int> vec = { 1, 2, 3 };

    SECTION("using normal iterators")
    {
        for(auto it = vec.begin(); it != vec.end(); ++it)
        {
            (*it)++;
        }

        auto expected = { 2, 3, 4 }; // std::initializer_list<int>

        REQUIRE(equal(vec.begin(), vec.end(), expected.begin(), expected.end()));
    }

    SECTION("read-only using const iterators")
    {
        for(auto it = vec.cbegin(); it != vec.cend(); ++it)
        {
            //(*it)++;  // compilation error
        }
    }
}

template <typename T>
void template_function(const T& t)
{

}

template <typename T, size_t N>
size_t deduce_size(T(&arr)[N])
{
    return N;
}

TEST_CASE("deduce_size")
{
    int tab1[10];
    int tab2[255];

    REQUIRE(deduce_size(tab1) == 10);
    REQUIRE(deduce_size(tab2) == 255);
}

TEST_CASE("auto - type deduction mechanism")
{
    template_function(19); // int
    template_function("text"); // const char*

    int x = 10;
    int& rx = x;
    const int cx = 12;

    SECTION("case 1 - auto with ref")
    {
        SECTION("ref modifier is preserved")
        {
            auto& a1 = rx;
            a1 = 11;
            REQUIRE(x == 11);
        }

        SECTION("const modifiers are respected")
        {
            auto& a1 = cx;
            static_assert(is_same<decltype(a1), const int&>::value, "Error");
            //a1++;  // compilation error
        }

        SECTION("array doesn't decay to pointer")
        {
            int tab[10];

            auto& tab2 = tab;
            static_assert(is_same<decltype(tab2), int(&)[10]>::value, "Error");
        }
    }

    SECTION("case 3 - auto without ref or pointer")
    {
        SECTION("ref are removed")
        {
            auto a1 = rx; // int
            static_assert(is_same<decltype(a1), int>::value, "Error");
            a1 = 22;
            REQUIRE(rx == 10);
        }

        SECTION("const are removed")
        {
            auto a1 = cx;
            static_assert(is_same<decltype(a1), int>::value, "Error");
            a1++;
            REQUIRE(a1 == 13);
        }

        SECTION("array decays to pointer")
        {
            int tab[10];

            auto tab2 = tab;
            static_assert(is_same<decltype(tab2), int*>::value, "Error");
        }
    }

    SECTION("extra case")
    {
        auto il = {1, 2, 3};

        static_assert(is_same<decltype(il), std::initializer_list<int>>::value, "Error");
    }
}

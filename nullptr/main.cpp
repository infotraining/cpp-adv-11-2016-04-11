#include <iostream>

using namespace std;

void foo(int* ptr)
{
    if (ptr)
        cout << "Value: " << *ptr << endl;
    else
        cout << "Pointer is NULL" << endl;
}

void foo(long value)
{
    cout << "Int value: " << value << endl;
}

void foo(nullptr_t)
{
    cout << "Case for nullptr" << endl;
}

int main()
{
    int x = 10;

    int* ptr_x = nullptr;

    if (ptr_x != nullptr)
        cout << *ptr_x << endl;

    foo(ptr_x);
    foo(nullptr);

    int* ptr2{};  // int* ptr2 = nullptr;
}

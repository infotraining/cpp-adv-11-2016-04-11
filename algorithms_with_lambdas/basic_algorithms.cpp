#include <vector>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <functional>
#include <cmath>
#include <numeric>
#include <random>

using namespace std;

int main()
{
    vector<int> vec(25);

    std::random_device rd;
    std::mt19937 mt {0};
    std::uniform_int_distribution<int> uniform_dist {1, 30};

    generate(vec.begin(), vec.end(), [&] { return uniform_dist(mt); } );

    cout << "vec: ";
    for(const auto& item : vec)
        cout << item <<" ";
    cout << endl;

    // 1a - wyświetl parzyste
    cout << "evens: ";
    //for_each(vec.begin(), vec.end(), [](int x) { if (x % 2 == 0) cout << x << " "; });
    auto is_even = [](int x) { return x % 2 == 0; };
    copy_if(vec.begin(), vec.end(), ostream_iterator<int>(cout, " "), is_even);
    cout << endl;

    // 1b - wyswietl ile jest parzystych
    cout << "count evens: "
         << count_if(vec.begin(), vec.end(), is_even)
         << endl;

    int eliminators[] = { 3, 5, 7 };
    // 2 - usuń liczby podzielne przez dowolną liczbę z tablicy eliminators
    auto garbage_starts
            = remove_if(vec.begin(), vec.end(),
                        [&eliminators](int x) {
                            return any_of(begin(eliminators), end(eliminators),
                                          [x](int n) { return x % n == 0; });});

    vec.erase(garbage_starts, vec.end());


    // 3 - tranformacja: podnieś liczby do kwadratu
    transform(vec.begin(), vec.end(), vec.begin(), [](int x) { return x * x; });

    // 4 - wypisz 5 najwiekszych liczb
    nth_element(vec.begin(), vec.begin() + 5, vec.end(), greater<int>());

    for(auto it = vec.begin(); it != vec.begin() + 5; ++it)
        cout << *it << " ";
    cout << endl;


    // 5 - policz wartosc srednia
    double sum = accumulate(vec.begin(), vec.end(), 0.0);
    double avg = sum/vec.size();

    // 6 - utwórz dwa kontenery - 1. z liczbami mniejszymi lub równymi średniej, 2. z liczbami większymi od średniej
    vector<int> lt_eq_avg;
    vector<int> gt_avg;

    partition_copy(vec.begin(), vec.end(), back_inserter(lt_eq_avg), back_inserter(gt_avg),
                   [avg](int x) { return x <= avg; });
}

#define CATCH_CONFIG_MAIN

#include <iostream>
#include <vector>
#include <list>
#include <boost/range/adaptors.hpp>
#include "catch.hpp"

using namespace std;

TEST_CASE("range-based-for")
{
    SECTION("allows iteration over container")
    {
        vector<int> vec = { 1, 2, 3, 4 };


        SECTION("using auto copies each item")
        {
            for(auto item : vec)
            {
                cout << item << " ";
                item++;
            }
            cout << endl;

            REQUIRE(vec[0] == 1);
        }

        SECTION("using auto& allows modifications")
        {
            for(auto& item : vec)
            {
                item++;
            }

            REQUIRE(vec[0] == 2);
            REQUIRE(vec.back() == 5);
        }

        SECTION("using const auto& - read-only iteration")
        {
            vector<string> words = { "one", "two", "three" };

            for(const auto& w : words)
                cout << w << " ";
            cout << endl;
        }

        SECTION("case1 - compiler unwraps to")
        {
            for(auto it = vec.begin(); it != vec.end(); ++it)
            {
                auto item = *it;

                cout << item << " ";
            }
            cout << endl;
        }
    }


    SECTION("allows iteration over native array")
    {
        int tab[5] = { 1, 2, 3 };

        for(const auto& item : tab)
        {
            cout << item << " ";
        }
        cout << endl;

        for(auto& item : tab)
            ++item;

        auto expected = { 2, 3, 4, 1, 1 };

        REQUIRE(equal(begin(tab), end(tab), expected.begin()));

        SECTION("case2 - compiler unwraps to")
        {
            for(auto it = begin(tab); it != end(tab); ++it)
            {
                auto item = *it;

                cout << item << " ";
            }
            cout << endl;
        }
    }

    SECTION("allows iteration over initializer list")
    {
        auto il = { 1, 2, 3 };

        for(const auto& item1 : il)
            for(const auto& item2 : { 1, 2, 3 })
                cout << "(" << item1 << ", " << item2 << ") ";
        cout << endl;
    }

    SECTION("boost.range allows range-based-for with index")
    {
        list<int> lst = { 1, 2, 3, 4 };

        using namespace boost::adaptors;

        for(const auto& item : lst | reversed | indexed(0))
        {
            cout << "(" << item.index() << ", " << item.value() << ") ";
        }
        cout << endl;
    }

    SECTION("allows iteration with &&")
    {
        vector<bool> vec = { 1, 0, 0, 0, 1 };

        for(auto&& item : vec)
        {
            item = !item;
        }

        for(const auto& item : vec)
        {
            cout << item << " ";
        }
        cout << endl;
    }
}

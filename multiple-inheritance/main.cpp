#include <iostream>

using namespace std;

class Vehicle
{
    string id_;
public:
    Vehicle()
    {
        cout << "Vehicle()" << endl;
    }

    Vehicle(const string& id) : id_{id}
    {
        cout << "Vehicle(" << id_ << ")" << endl;
    }

    string id() const
    {
        return id_;
    }

    virtual void travel(size_t dist) = 0;

    virtual void info() const
    {
        cout << "Id: " << id() << endl;
    }

    virtual ~Vehicle() = default;
protected:
    void set_id(const string& new_id)
    {
        id_ = new_id;
    }
};

class Car : public virtual Vehicle
{
    size_t distance_km_ {};
protected:
    Car() = default;
public:

    Car(const string& id) : Vehicle{id}, distance_km_{0}
    {
        cout << "Car(" << this->id() << ")" << endl;
    }

    void travel(size_t dist) override
    {
        distance_km_ += dist;
    }

    void info() const override
    {
        Vehicle::info();
        cout << "km: " << distance_km_ << endl;
    }
};

class Boat : public virtual  Vehicle
{
    size_t distance_sm_ {};
protected:
    Boat() = default;
public:
    Boat(const string& id) : distance_sm_{0}
    {
        cout << "Boat(" << this->id() << ")" << endl;
    }

    void travel(size_t dist) override
    {
        distance_sm_ += dist;
    }

    void info() const override
    {
        Vehicle::info();
        cout << "sm: " << distance_sm_ << endl;
    }
};

class Amph : public Car, public Boat
{
public:
    Amph(string id)
    {
        set_id(id);
    }

    void info() const override
    {
        Car::info();
        Boat::info();
    }

    void travel(size_t dist) override
    {
        Car::travel(dist/2);
        Boat::travel(dist/2);
    }
};

int main()
{
    Amph amph{"A1"};

    amph.info();

    amph.Car::travel(1000);
    amph.Boat::travel(200);

    amph.info();

    Car& c = amph;
    c.travel(500);

    amph.info();
}

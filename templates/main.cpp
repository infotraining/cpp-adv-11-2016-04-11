#include <iostream>
#include <cstring>
#include <vector>

using namespace std;

template <typename T>
const T& maximum(const T& a, const T& b)
{
    cout << "generic version" << endl;
    return a < b ? b : a;
}

template <typename T>
T* maximum(T* a, T* b)
{
    cout << "pointer version" << endl;
    return *a < *b ? b : a;
}

const char* maximum(const char* a, const char* b)
{
    cout << "const char* version" << endl;
    return strcmp(a, b) < 0 ? b : a;
}

template <typename T, size_t N = 255>
class Array
{
    T items_[N];
public:
    using iterator = T*;
    using const_iterator = const T*;

    Array(const T& init_value = T())
    {
        fill(std::begin(items_), std::end(items_), init_value);
    }

    size_t size() const
    {
        return N;
    }

    T& operator[](size_t index)
    {
        return items_[index];
    }

    const T& operator[](size_t index) const;

    iterator begin()
    {
        return items_;
    }

    iterator end()
    {
        return items_ + N;
    }

    const_iterator begin() const
    {
        return items_;
    }

    const_iterator end() const
    {
        return items_ + N;
    }
};

template <typename T, size_t N>
const T& Array<T, N>::operator[](size_t index) const
{
    return items_[index];
}


// partial specialization for pointer
template <typename T, size_t N>
class Array<T*, N>
{
public:
    T* items_[N];
public:
    typedef T* value_type;
    using iterator = value_type*;
    using const_iterator = const iterator;

    Array()
    {
        fill(std::begin(items_), std::end(items_), nullptr);
    }

    ~Array()
    {
        for(auto ptr : items_)
            delete ptr;
    }

    size_t size() const
    {
        return N;
    }

    T*& operator[](size_t index)
    {
        return items_[index];
    }

    const T& operator[](size_t index) const
    {
        return items_[index];
    }

    iterator begin()
    {
        return items_;
    }

    iterator end()
    {
        return items_ + N;
    }

    const_iterator begin() const
    {
        return items_;
    }

    const_iterator end() const
    {
        return items_ + N;
    }
};

namespace other
{
    template
        <
         typename T,
         template <typename, typename> class Container,
         typename Allocator = std::allocator<T>
        >
    class Array
    {
        Container<T, Allocator> items_;
    public:
        using iterator = typename Container<T, Allocator>::iterator;
        using const_iterator = typename Container<T, Allocator>::const_iterator;

        Array(size_t size, const T& init_value = T()) : items_(size)
        {
            fill(std::begin(items_), std::end(items_), init_value);
        }

        size_t size() const
        {
            return items_.size();
        }

        T& operator[](size_t index)
        {
            return items_[index];
        }

        const T& operator[](size_t index) const;

        iterator begin()
        {
            return std::begin(items_);
        }

        iterator end()
        {
            return std::end(items_);
        }

        const_iterator begin() const
        {
            return std::begin(items_);
        }

        const_iterator end() const
        {
            return std::end(items_);
        }
    };

    template <typename T, template<typename, typename> class Container, typename Allocator>
    const T& Array<T, Container, Allocator>::operator[](size_t index) const
    {
        return items_[index];
    }
}

struct Foo
{
    Foo()
    {
        cout << "Foo()" << endl;
    }

    ~Foo()
    {
        cout << "~Foo()" << endl;
    }
};

int main()
{
    const int& (*ptr_f)(const int&, const int&) = &maximum<int>;

    int a = 20;
    int b = 10;

    cout << maximum(a, b) << endl;
    cout << maximum(6.5, 23.5) << endl;
    cout << maximum(static_cast<double>(a), 6.5) << endl;
    cout << maximum<double>(a, 6.5) << endl;

    int* ptra = &a;
    int* ptrb = &b;

    auto ptr_result = maximum(ptra, ptrb);
    cout << *ptr_result << endl;

    auto txt1 = "aaa";
    auto txt2 = "aba";

    cout << maximum(txt1, txt2) << endl;

    Array<int> arr_int;
    for(const auto& item : arr_int)
        cout << item << " ";
    cout << endl;


    Array<double, 10> arr_d(3.14);
    for(const auto& item : arr_d)
        cout << item << " ";
    cout << endl;

    Array<double, 10> arr_d2 = arr_d;
    for(const auto& item : arr_d2)
        cout << item << " ";
    cout << endl;

    other::Array<int, vector> other_arr(10, 8);
    for(const auto& item : other_arr)
        cout << item << " ";
    cout << endl;

    Array<Foo*> arr_ptrs;

    arr_ptrs[0] = new Foo[10];
    arr_ptrs[2] = new Foo();
}
